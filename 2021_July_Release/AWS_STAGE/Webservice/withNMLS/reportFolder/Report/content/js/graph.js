/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
$(document).ready(function() {

    $(".click-title").mouseenter( function(    e){
        e.preventDefault();
        this.style.cursor="pointer";
    });
    $(".click-title").mousedown( function(event){
        event.preventDefault();
    });

    // Ugly code while this script is shared among several pages
    try{
        refreshHitsPerSecond(true);
    } catch(e){}
    try{
        refreshResponseTimeOverTime(true);
    } catch(e){}
    try{
        refreshResponseTimePercentiles();
    } catch(e){}
});


var responseTimePercentilesInfos = {
        data: {"result": {"minY": 429.0, "minX": 0.0, "maxY": 21518.0, "series": [{"data": [[0.0, 429.0], [0.1, 433.0], [0.2, 433.0], [0.3, 434.0], [0.4, 435.0], [0.5, 435.0], [0.6, 436.0], [0.7, 436.0], [0.8, 436.0], [0.9, 437.0], [1.0, 437.0], [1.1, 437.0], [1.2, 437.0], [1.3, 437.0], [1.4, 437.0], [1.5, 438.0], [1.6, 438.0], [1.7, 438.0], [1.8, 438.0], [1.9, 439.0], [2.0, 439.0], [2.1, 439.0], [2.2, 439.0], [2.3, 439.0], [2.4, 439.0], [2.5, 439.0], [2.6, 439.0], [2.7, 440.0], [2.8, 440.0], [2.9, 440.0], [3.0, 440.0], [3.1, 440.0], [3.2, 440.0], [3.3, 441.0], [3.4, 441.0], [3.5, 441.0], [3.6, 441.0], [3.7, 441.0], [3.8, 441.0], [3.9, 441.0], [4.0, 441.0], [4.1, 442.0], [4.2, 442.0], [4.3, 442.0], [4.4, 442.0], [4.5, 442.0], [4.6, 442.0], [4.7, 442.0], [4.8, 442.0], [4.9, 442.0], [5.0, 442.0], [5.1, 442.0], [5.2, 443.0], [5.3, 443.0], [5.4, 443.0], [5.5, 443.0], [5.6, 443.0], [5.7, 443.0], [5.8, 443.0], [5.9, 443.0], [6.0, 443.0], [6.1, 443.0], [6.2, 443.0], [6.3, 443.0], [6.4, 443.0], [6.5, 444.0], [6.6, 444.0], [6.7, 444.0], [6.8, 444.0], [6.9, 444.0], [7.0, 444.0], [7.1, 444.0], [7.2, 444.0], [7.3, 444.0], [7.4, 444.0], [7.5, 444.0], [7.6, 444.0], [7.7, 444.0], [7.8, 445.0], [7.9, 445.0], [8.0, 445.0], [8.1, 445.0], [8.2, 445.0], [8.3, 445.0], [8.4, 445.0], [8.5, 445.0], [8.6, 445.0], [8.7, 445.0], [8.8, 445.0], [8.9, 445.0], [9.0, 445.0], [9.1, 445.0], [9.2, 445.0], [9.3, 446.0], [9.4, 446.0], [9.5, 446.0], [9.6, 446.0], [9.7, 446.0], [9.8, 446.0], [9.9, 446.0], [10.0, 446.0], [10.1, 446.0], [10.2, 446.0], [10.3, 446.0], [10.4, 446.0], [10.5, 446.0], [10.6, 447.0], [10.7, 447.0], [10.8, 447.0], [10.9, 447.0], [11.0, 447.0], [11.1, 447.0], [11.2, 447.0], [11.3, 447.0], [11.4, 447.0], [11.5, 447.0], [11.6, 447.0], [11.7, 447.0], [11.8, 447.0], [11.9, 447.0], [12.0, 448.0], [12.1, 448.0], [12.2, 448.0], [12.3, 448.0], [12.4, 448.0], [12.5, 448.0], [12.6, 448.0], [12.7, 448.0], [12.8, 448.0], [12.9, 448.0], [13.0, 448.0], [13.1, 448.0], [13.2, 448.0], [13.3, 448.0], [13.4, 448.0], [13.5, 448.0], [13.6, 448.0], [13.7, 449.0], [13.8, 449.0], [13.9, 449.0], [14.0, 449.0], [14.1, 449.0], [14.2, 449.0], [14.3, 449.0], [14.4, 449.0], [14.5, 449.0], [14.6, 449.0], [14.7, 449.0], [14.8, 449.0], [14.9, 449.0], [15.0, 449.0], [15.1, 449.0], [15.2, 449.0], [15.3, 449.0], [15.4, 449.0], [15.5, 449.0], [15.6, 450.0], [15.7, 450.0], [15.8, 450.0], [15.9, 450.0], [16.0, 450.0], [16.1, 450.0], [16.2, 450.0], [16.3, 450.0], [16.4, 450.0], [16.5, 450.0], [16.6, 450.0], [16.7, 450.0], [16.8, 450.0], [16.9, 450.0], [17.0, 450.0], [17.1, 450.0], [17.2, 450.0], [17.3, 450.0], [17.4, 450.0], [17.5, 451.0], [17.6, 451.0], [17.7, 451.0], [17.8, 451.0], [17.9, 451.0], [18.0, 451.0], [18.1, 451.0], [18.2, 451.0], [18.3, 451.0], [18.4, 451.0], [18.5, 451.0], [18.6, 451.0], [18.7, 451.0], [18.8, 451.0], [18.9, 451.0], [19.0, 451.0], [19.1, 451.0], [19.2, 451.0], [19.3, 452.0], [19.4, 452.0], [19.5, 452.0], [19.6, 452.0], [19.7, 452.0], [19.8, 452.0], [19.9, 452.0], [20.0, 452.0], [20.1, 452.0], [20.2, 452.0], [20.3, 452.0], [20.4, 452.0], [20.5, 452.0], [20.6, 452.0], [20.7, 452.0], [20.8, 452.0], [20.9, 452.0], [21.0, 452.0], [21.1, 452.0], [21.2, 452.0], [21.3, 452.0], [21.4, 452.0], [21.5, 452.0], [21.6, 452.0], [21.7, 453.0], [21.8, 453.0], [21.9, 453.0], [22.0, 453.0], [22.1, 453.0], [22.2, 453.0], [22.3, 453.0], [22.4, 453.0], [22.5, 453.0], [22.6, 453.0], [22.7, 453.0], [22.8, 453.0], [22.9, 453.0], [23.0, 453.0], [23.1, 453.0], [23.2, 453.0], [23.3, 453.0], [23.4, 453.0], [23.5, 453.0], [23.6, 453.0], [23.7, 454.0], [23.8, 454.0], [23.9, 454.0], [24.0, 454.0], [24.1, 454.0], [24.2, 454.0], [24.3, 454.0], [24.4, 454.0], [24.5, 454.0], [24.6, 454.0], [24.7, 454.0], [24.8, 454.0], [24.9, 454.0], [25.0, 454.0], [25.1, 454.0], [25.2, 454.0], [25.3, 454.0], [25.4, 454.0], [25.5, 455.0], [25.6, 455.0], [25.7, 455.0], [25.8, 455.0], [25.9, 455.0], [26.0, 455.0], [26.1, 455.0], [26.2, 455.0], [26.3, 455.0], [26.4, 455.0], [26.5, 455.0], [26.6, 455.0], [26.7, 455.0], [26.8, 455.0], [26.9, 455.0], [27.0, 455.0], [27.1, 455.0], [27.2, 455.0], [27.3, 455.0], [27.4, 455.0], [27.5, 455.0], [27.6, 455.0], [27.7, 456.0], [27.8, 456.0], [27.9, 456.0], [28.0, 456.0], [28.1, 456.0], [28.2, 456.0], [28.3, 456.0], [28.4, 456.0], [28.5, 456.0], [28.6, 456.0], [28.7, 456.0], [28.8, 456.0], [28.9, 456.0], [29.0, 456.0], [29.1, 456.0], [29.2, 456.0], [29.3, 456.0], [29.4, 456.0], [29.5, 456.0], [29.6, 456.0], [29.7, 456.0], [29.8, 456.0], [29.9, 457.0], [30.0, 457.0], [30.1, 457.0], [30.2, 457.0], [30.3, 457.0], [30.4, 457.0], [30.5, 457.0], [30.6, 457.0], [30.7, 457.0], [30.8, 457.0], [30.9, 457.0], [31.0, 457.0], [31.1, 457.0], [31.2, 457.0], [31.3, 457.0], [31.4, 457.0], [31.5, 457.0], [31.6, 457.0], [31.7, 457.0], [31.8, 458.0], [31.9, 458.0], [32.0, 458.0], [32.1, 458.0], [32.2, 458.0], [32.3, 458.0], [32.4, 458.0], [32.5, 458.0], [32.6, 458.0], [32.7, 458.0], [32.8, 458.0], [32.9, 458.0], [33.0, 458.0], [33.1, 458.0], [33.2, 458.0], [33.3, 458.0], [33.4, 458.0], [33.5, 458.0], [33.6, 458.0], [33.7, 458.0], [33.8, 459.0], [33.9, 459.0], [34.0, 459.0], [34.1, 459.0], [34.2, 459.0], [34.3, 459.0], [34.4, 459.0], [34.5, 459.0], [34.6, 459.0], [34.7, 459.0], [34.8, 459.0], [34.9, 459.0], [35.0, 459.0], [35.1, 459.0], [35.2, 459.0], [35.3, 459.0], [35.4, 459.0], [35.5, 459.0], [35.6, 459.0], [35.7, 460.0], [35.8, 460.0], [35.9, 460.0], [36.0, 460.0], [36.1, 460.0], [36.2, 460.0], [36.3, 460.0], [36.4, 460.0], [36.5, 460.0], [36.6, 460.0], [36.7, 460.0], [36.8, 460.0], [36.9, 460.0], [37.0, 460.0], [37.1, 460.0], [37.2, 460.0], [37.3, 460.0], [37.4, 460.0], [37.5, 460.0], [37.6, 460.0], [37.7, 461.0], [37.8, 461.0], [37.9, 461.0], [38.0, 461.0], [38.1, 461.0], [38.2, 461.0], [38.3, 461.0], [38.4, 461.0], [38.5, 461.0], [38.6, 461.0], [38.7, 461.0], [38.8, 461.0], [38.9, 461.0], [39.0, 461.0], [39.1, 461.0], [39.2, 461.0], [39.3, 461.0], [39.4, 461.0], [39.5, 461.0], [39.6, 461.0], [39.7, 461.0], [39.8, 461.0], [39.9, 461.0], [40.0, 461.0], [40.1, 462.0], [40.2, 462.0], [40.3, 462.0], [40.4, 462.0], [40.5, 462.0], [40.6, 462.0], [40.7, 462.0], [40.8, 462.0], [40.9, 462.0], [41.0, 462.0], [41.1, 462.0], [41.2, 462.0], [41.3, 462.0], [41.4, 462.0], [41.5, 462.0], [41.6, 462.0], [41.7, 462.0], [41.8, 462.0], [41.9, 462.0], [42.0, 463.0], [42.1, 463.0], [42.2, 463.0], [42.3, 463.0], [42.4, 463.0], [42.5, 463.0], [42.6, 463.0], [42.7, 463.0], [42.8, 463.0], [42.9, 463.0], [43.0, 463.0], [43.1, 463.0], [43.2, 463.0], [43.3, 463.0], [43.4, 463.0], [43.5, 464.0], [43.6, 464.0], [43.7, 464.0], [43.8, 464.0], [43.9, 464.0], [44.0, 464.0], [44.1, 464.0], [44.2, 464.0], [44.3, 464.0], [44.4, 464.0], [44.5, 464.0], [44.6, 464.0], [44.7, 464.0], [44.8, 464.0], [44.9, 464.0], [45.0, 464.0], [45.1, 464.0], [45.2, 465.0], [45.3, 465.0], [45.4, 465.0], [45.5, 465.0], [45.6, 465.0], [45.7, 465.0], [45.8, 465.0], [45.9, 465.0], [46.0, 465.0], [46.1, 465.0], [46.2, 465.0], [46.3, 465.0], [46.4, 465.0], [46.5, 465.0], [46.6, 465.0], [46.7, 465.0], [46.8, 466.0], [46.9, 466.0], [47.0, 466.0], [47.1, 466.0], [47.2, 466.0], [47.3, 466.0], [47.4, 466.0], [47.5, 466.0], [47.6, 466.0], [47.7, 466.0], [47.8, 466.0], [47.9, 466.0], [48.0, 466.0], [48.1, 466.0], [48.2, 466.0], [48.3, 466.0], [48.4, 466.0], [48.5, 466.0], [48.6, 466.0], [48.7, 467.0], [48.8, 467.0], [48.9, 467.0], [49.0, 467.0], [49.1, 467.0], [49.2, 467.0], [49.3, 467.0], [49.4, 467.0], [49.5, 467.0], [49.6, 467.0], [49.7, 467.0], [49.8, 467.0], [49.9, 467.0], [50.0, 467.0], [50.1, 467.0], [50.2, 468.0], [50.3, 468.0], [50.4, 468.0], [50.5, 468.0], [50.6, 468.0], [50.7, 468.0], [50.8, 468.0], [50.9, 468.0], [51.0, 468.0], [51.1, 468.0], [51.2, 468.0], [51.3, 468.0], [51.4, 468.0], [51.5, 468.0], [51.6, 468.0], [51.7, 468.0], [51.8, 469.0], [51.9, 469.0], [52.0, 469.0], [52.1, 469.0], [52.2, 469.0], [52.3, 469.0], [52.4, 469.0], [52.5, 469.0], [52.6, 469.0], [52.7, 469.0], [52.8, 469.0], [52.9, 469.0], [53.0, 469.0], [53.1, 470.0], [53.2, 470.0], [53.3, 470.0], [53.4, 470.0], [53.5, 470.0], [53.6, 470.0], [53.7, 470.0], [53.8, 470.0], [53.9, 470.0], [54.0, 470.0], [54.1, 470.0], [54.2, 470.0], [54.3, 470.0], [54.4, 471.0], [54.5, 471.0], [54.6, 471.0], [54.7, 471.0], [54.8, 471.0], [54.9, 471.0], [55.0, 471.0], [55.1, 471.0], [55.2, 471.0], [55.3, 471.0], [55.4, 471.0], [55.5, 471.0], [55.6, 471.0], [55.7, 471.0], [55.8, 472.0], [55.9, 472.0], [56.0, 472.0], [56.1, 472.0], [56.2, 472.0], [56.3, 472.0], [56.4, 472.0], [56.5, 472.0], [56.6, 472.0], [56.7, 472.0], [56.8, 472.0], [56.9, 472.0], [57.0, 472.0], [57.1, 473.0], [57.2, 473.0], [57.3, 473.0], [57.4, 473.0], [57.5, 473.0], [57.6, 473.0], [57.7, 473.0], [57.8, 473.0], [57.9, 473.0], [58.0, 473.0], [58.1, 473.0], [58.2, 473.0], [58.3, 473.0], [58.4, 474.0], [58.5, 474.0], [58.6, 474.0], [58.7, 474.0], [58.8, 474.0], [58.9, 474.0], [59.0, 474.0], [59.1, 474.0], [59.2, 474.0], [59.3, 475.0], [59.4, 475.0], [59.5, 475.0], [59.6, 475.0], [59.7, 475.0], [59.8, 475.0], [59.9, 475.0], [60.0, 475.0], [60.1, 476.0], [60.2, 476.0], [60.3, 476.0], [60.4, 476.0], [60.5, 476.0], [60.6, 476.0], [60.7, 476.0], [60.8, 476.0], [60.9, 476.0], [61.0, 476.0], [61.1, 477.0], [61.2, 477.0], [61.3, 477.0], [61.4, 477.0], [61.5, 477.0], [61.6, 477.0], [61.7, 477.0], [61.8, 477.0], [61.9, 477.0], [62.0, 477.0], [62.1, 478.0], [62.2, 478.0], [62.3, 478.0], [62.4, 478.0], [62.5, 478.0], [62.6, 478.0], [62.7, 478.0], [62.8, 479.0], [62.9, 479.0], [63.0, 479.0], [63.1, 479.0], [63.2, 479.0], [63.3, 479.0], [63.4, 479.0], [63.5, 479.0], [63.6, 480.0], [63.7, 480.0], [63.8, 480.0], [63.9, 480.0], [64.0, 480.0], [64.1, 480.0], [64.2, 480.0], [64.3, 480.0], [64.4, 480.0], [64.5, 480.0], [64.6, 480.0], [64.7, 481.0], [64.8, 481.0], [64.9, 481.0], [65.0, 481.0], [65.1, 481.0], [65.2, 481.0], [65.3, 481.0], [65.4, 481.0], [65.5, 481.0], [65.6, 481.0], [65.7, 482.0], [65.8, 482.0], [65.9, 482.0], [66.0, 482.0], [66.1, 482.0], [66.2, 482.0], [66.3, 482.0], [66.4, 482.0], [66.5, 483.0], [66.6, 483.0], [66.7, 483.0], [66.8, 483.0], [66.9, 483.0], [67.0, 483.0], [67.1, 483.0], [67.2, 483.0], [67.3, 483.0], [67.4, 484.0], [67.5, 484.0], [67.6, 484.0], [67.7, 484.0], [67.8, 484.0], [67.9, 484.0], [68.0, 485.0], [68.1, 485.0], [68.2, 485.0], [68.3, 485.0], [68.4, 485.0], [68.5, 485.0], [68.6, 485.0], [68.7, 485.0], [68.8, 485.0], [68.9, 485.0], [69.0, 485.0], [69.1, 486.0], [69.2, 486.0], [69.3, 486.0], [69.4, 486.0], [69.5, 486.0], [69.6, 486.0], [69.7, 487.0], [69.8, 487.0], [69.9, 487.0], [70.0, 487.0], [70.1, 487.0], [70.2, 488.0], [70.3, 488.0], [70.4, 488.0], [70.5, 488.0], [70.6, 488.0], [70.7, 488.0], [70.8, 489.0], [70.9, 489.0], [71.0, 489.0], [71.1, 489.0], [71.2, 489.0], [71.3, 489.0], [71.4, 489.0], [71.5, 489.0], [71.6, 490.0], [71.7, 490.0], [71.8, 491.0], [71.9, 491.0], [72.0, 491.0], [72.1, 491.0], [72.2, 491.0], [72.3, 492.0], [72.4, 492.0], [72.5, 492.0], [72.6, 492.0], [72.7, 492.0], [72.8, 492.0], [72.9, 493.0], [73.0, 493.0], [73.1, 493.0], [73.2, 494.0], [73.3, 494.0], [73.4, 494.0], [73.5, 494.0], [73.6, 494.0], [73.7, 494.0], [73.8, 495.0], [73.9, 495.0], [74.0, 495.0], [74.1, 495.0], [74.2, 496.0], [74.3, 496.0], [74.4, 496.0], [74.5, 496.0], [74.6, 496.0], [74.7, 496.0], [74.8, 497.0], [74.9, 497.0], [75.0, 497.0], [75.1, 497.0], [75.2, 497.0], [75.3, 497.0], [75.4, 497.0], [75.5, 497.0], [75.6, 498.0], [75.7, 498.0], [75.8, 498.0], [75.9, 498.0], [76.0, 498.0], [76.1, 499.0], [76.2, 499.0], [76.3, 499.0], [76.4, 499.0], [76.5, 499.0], [76.6, 500.0], [76.7, 500.0], [76.8, 500.0], [76.9, 500.0], [77.0, 500.0], [77.1, 500.0], [77.2, 500.0], [77.3, 501.0], [77.4, 501.0], [77.5, 501.0], [77.6, 501.0], [77.7, 501.0], [77.8, 501.0], [77.9, 502.0], [78.0, 502.0], [78.1, 502.0], [78.2, 502.0], [78.3, 502.0], [78.4, 502.0], [78.5, 502.0], [78.6, 503.0], [78.7, 503.0], [78.8, 503.0], [78.9, 503.0], [79.0, 503.0], [79.1, 503.0], [79.2, 504.0], [79.3, 504.0], [79.4, 504.0], [79.5, 504.0], [79.6, 505.0], [79.7, 505.0], [79.8, 505.0], [79.9, 505.0], [80.0, 505.0], [80.1, 506.0], [80.2, 506.0], [80.3, 506.0], [80.4, 506.0], [80.5, 507.0], [80.6, 507.0], [80.7, 507.0], [80.8, 507.0], [80.9, 507.0], [81.0, 508.0], [81.1, 508.0], [81.2, 509.0], [81.3, 509.0], [81.4, 509.0], [81.5, 509.0], [81.6, 509.0], [81.7, 509.0], [81.8, 509.0], [81.9, 510.0], [82.0, 510.0], [82.1, 510.0], [82.2, 510.0], [82.3, 511.0], [82.4, 511.0], [82.5, 512.0], [82.6, 512.0], [82.7, 512.0], [82.8, 512.0], [82.9, 513.0], [83.0, 513.0], [83.1, 513.0], [83.2, 514.0], [83.3, 514.0], [83.4, 514.0], [83.5, 514.0], [83.6, 514.0], [83.7, 515.0], [83.8, 515.0], [83.9, 515.0], [84.0, 515.0], [84.1, 515.0], [84.2, 516.0], [84.3, 516.0], [84.4, 516.0], [84.5, 516.0], [84.6, 516.0], [84.7, 517.0], [84.8, 517.0], [84.9, 517.0], [85.0, 517.0], [85.1, 518.0], [85.2, 518.0], [85.3, 518.0], [85.4, 518.0], [85.5, 518.0], [85.6, 519.0], [85.7, 519.0], [85.8, 519.0], [85.9, 519.0], [86.0, 519.0], [86.1, 520.0], [86.2, 520.0], [86.3, 521.0], [86.4, 521.0], [86.5, 521.0], [86.6, 522.0], [86.7, 522.0], [86.8, 522.0], [86.9, 522.0], [87.0, 522.0], [87.1, 523.0], [87.2, 523.0], [87.3, 524.0], [87.4, 525.0], [87.5, 525.0], [87.6, 526.0], [87.7, 526.0], [87.8, 526.0], [87.9, 527.0], [88.0, 527.0], [88.1, 528.0], [88.2, 528.0], [88.3, 529.0], [88.4, 529.0], [88.5, 529.0], [88.6, 530.0], [88.7, 530.0], [88.8, 530.0], [88.9, 531.0], [89.0, 531.0], [89.1, 531.0], [89.2, 532.0], [89.3, 533.0], [89.4, 533.0], [89.5, 533.0], [89.6, 534.0], [89.7, 534.0], [89.8, 535.0], [89.9, 535.0], [90.0, 535.0], [90.1, 536.0], [90.2, 536.0], [90.3, 536.0], [90.4, 536.0], [90.5, 536.0], [90.6, 537.0], [90.7, 538.0], [90.8, 538.0], [90.9, 538.0], [91.0, 538.0], [91.1, 539.0], [91.2, 539.0], [91.3, 540.0], [91.4, 540.0], [91.5, 540.0], [91.6, 540.0], [91.7, 541.0], [91.8, 541.0], [91.9, 542.0], [92.0, 542.0], [92.1, 542.0], [92.2, 543.0], [92.3, 543.0], [92.4, 543.0], [92.5, 543.0], [92.6, 545.0], [92.7, 545.0], [92.8, 546.0], [92.9, 547.0], [93.0, 547.0], [93.1, 548.0], [93.2, 548.0], [93.3, 549.0], [93.4, 549.0], [93.5, 550.0], [93.6, 550.0], [93.7, 551.0], [93.8, 552.0], [93.9, 553.0], [94.0, 554.0], [94.1, 555.0], [94.2, 556.0], [94.3, 556.0], [94.4, 557.0], [94.5, 558.0], [94.6, 559.0], [94.7, 559.0], [94.8, 559.0], [94.9, 560.0], [95.0, 561.0], [95.1, 561.0], [95.2, 562.0], [95.3, 563.0], [95.4, 563.0], [95.5, 564.0], [95.6, 564.0], [95.7, 566.0], [95.8, 566.0], [95.9, 567.0], [96.0, 568.0], [96.1, 569.0], [96.2, 569.0], [96.3, 570.0], [96.4, 572.0], [96.5, 574.0], [96.6, 574.0], [96.7, 575.0], [96.8, 575.0], [96.9, 576.0], [97.0, 578.0], [97.1, 581.0], [97.2, 584.0], [97.3, 587.0], [97.4, 589.0], [97.5, 590.0], [97.6, 592.0], [97.7, 596.0], [97.8, 598.0], [97.9, 604.0], [98.0, 606.0], [98.1, 608.0], [98.2, 612.0], [98.3, 616.0], [98.4, 622.0], [98.5, 639.0], [98.6, 676.0], [98.7, 689.0], [98.8, 706.0], [98.9, 826.0], [99.0, 858.0], [99.1, 913.0], [99.2, 925.0], [99.3, 950.0], [99.4, 982.0], [99.5, 1026.0], [99.6, 1048.0], [99.7, 1235.0], [99.8, 1471.0], [99.9, 3709.0], [100.0, 21518.0]], "isOverall": false, "label": "webservice_request", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 100.0, "title": "Response Time Percentiles"}},
        getOptions: function() {
            return {
                series: {
                    points: { show: false }
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentiles'
                },
                xaxis: {
                    tickDecimals: 1,
                    axisLabel: "Percentiles",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Percentile value in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : %x.2 percentile was %y ms"
                },
                selection: { mode: "xy" },
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentiles"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesPercentiles"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesPercentiles"), dataset, prepareOverviewOptions(options));
        }
};

/**
 * @param elementId Id of element where we display message
 */
function setEmptyGraph(elementId) {
    $(function() {
        $(elementId).text("No graph series with filter="+seriesFilter);
    });
}

// Response times percentiles
function refreshResponseTimePercentiles() {
    var infos = responseTimePercentilesInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimePercentiles");
        return;
    }
    if (isGraph($("#flotResponseTimesPercentiles"))){
        infos.createGraph();
    } else {
        var choiceContainer = $("#choicesResponseTimePercentiles");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesPercentiles", "#overviewResponseTimesPercentiles");
        $('#bodyResponseTimePercentiles .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimeDistributionInfos = {
        data: {"result": {"minY": 1.0, "minX": 400.0, "maxY": 2399.0, "series": [{"data": [[600.0, 28.0], [700.0, 4.0], [800.0, 5.0], [900.0, 12.0], [3600.0, 1.0], [3700.0, 1.0], [15700.0, 2.0], [1000.0, 7.0], [1100.0, 1.0], [1200.0, 1.0], [21500.0, 1.0], [1400.0, 3.0], [400.0, 2399.0], [1800.0, 1.0], [500.0, 668.0]], "isOverall": false, "label": "webservice_request", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 100, "maxX": 21500.0, "title": "Response Time Distribution"}},
        getOptions: function() {
            var granularity = this.data.result.granularity;
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    barWidth: this.data.result.granularity
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " responses for " + label + " were between " + xval + " and " + (xval + granularity) + " ms";
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimeDistribution"), prepareData(data.result.series, $("#choicesResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshResponseTimeDistribution() {
    var infos = responseTimeDistributionInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeDistribution");
        return;
    }
    if (isGraph($("#flotResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var syntheticResponseTimeDistributionInfos = {
        data: {"result": {"minY": 3134.0, "minX": 3.0, "ticks": [[0, "Requests having \nresponse time <= 500ms"], [1, "Requests having \nresponse time > 500ms and <= 1,500ms"], [2, "Requests having \nresponse time > 1,500ms"], [3, "Requests in error"]], "maxY": 3134.0, "series": [{"data": [], "color": "#9ACD32", "isOverall": false, "label": "Requests having \nresponse time <= 500ms", "isController": false}, {"data": [], "color": "yellow", "isOverall": false, "label": "Requests having \nresponse time > 500ms and <= 1,500ms", "isController": false}, {"data": [], "color": "orange", "isOverall": false, "label": "Requests having \nresponse time > 1,500ms", "isController": false}, {"data": [[3.0, 3134.0]], "color": "#FF6347", "isOverall": false, "label": "Requests in error", "isController": false}], "supportsControllersDiscrimination": false, "maxX": 3.0, "title": "Synthetic Response Times Distribution"}},
        getOptions: function() {
            return {
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendSyntheticResponseTimeDistribution'
                },
                xaxis:{
                    axisLabel: "Response times ranges",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                    tickLength:0,
                    min:-0.5,
                    max:3.5
                },
                yaxis: {
                    axisLabel: "Number of responses",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                bars : {
                    show: true,
                    align: "center",
                    barWidth: 0.25,
                    fill:.75
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: function(label, xval, yval, flotItem){
                        return yval + " " + label;
                    }
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var options = this.getOptions();
            prepareOptions(options, data);
            options.xaxis.ticks = data.result.ticks;
            $.plot($("#flotSyntheticResponseTimeDistribution"), prepareData(data.result.series, $("#choicesSyntheticResponseTimeDistribution")), options);
        }

};

// Response time distribution
function refreshSyntheticResponseTimeDistribution() {
    var infos = syntheticResponseTimeDistributionInfos;
    prepareSeries(infos.data, true);
    if (isGraph($("#flotSyntheticResponseTimeDistribution"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        $('#footerSyntheticResponseTimeDistribution .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var activeThreadsOverTimeInfos = {
        data: {"result": {"minY": 8.373382624768949, "minX": 1.63577886E12, "maxY": 22.57282693813627, "series": [{"data": [[1.63577892E12, 22.57282693813627], [1.63577886E12, 8.373382624768949], [1.63577898E12, 19.12820512820513]], "isOverall": false, "label": "Webservice", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.63577898E12, "title": "Active Threads Over Time"}},
        getOptions: function() {
            return {
                series: {
                    stack: true,
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 6,
                    show: true,
                    container: '#legendActiveThreadsOverTime'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                selection: {
                    mode: 'xy'
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : At %x there were %y active threads"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesActiveThreadsOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotActiveThreadsOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewActiveThreadsOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Active Threads Over Time
function refreshActiveThreadsOverTime(fixTimestamps) {
    var infos = activeThreadsOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, -25200000);
    }
    if(isGraph($("#flotActiveThreadsOverTime"))) {
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesActiveThreadsOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotActiveThreadsOverTime", "#overviewActiveThreadsOverTime");
        $('#footerActiveThreadsOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var timeVsThreadsInfos = {
        data: {"result": {"minY": 454.6551724137931, "minX": 1.0, "maxY": 6164.333333333333, "series": [{"data": [[2.0, 1723.6923076923076], [3.0, 610.9090909090909], [4.0, 461.26666666666665], [5.0, 461.50000000000006], [6.0, 467.4318181818182], [7.0, 465.24], [8.0, 454.6551724137931], [9.0, 459.76470588235304], [10.0, 455.52777777777777], [11.0, 467.62337662337666], [12.0, 482.63953488372096], [13.0, 466.20212765957444], [14.0, 468.9795918367346], [15.0, 465.2056074766356], [1.0, 6164.333333333333], [16.0, 459.61946902654876], [17.0, 461.2605042016806], [18.0, 462.52459016393453], [19.0, 483.7874015748033], [20.0, 539.8205128205126], [21.0, 467.161971830986], [22.0, 610.8333333333334], [23.0, 500.4090909090908], [24.0, 506.35714285714283], [25.0, 566.7284768211917], [26.0, 474.8486486486486], [27.0, 472.7326203208554], [28.0, 495.1767955801104], [29.0, 520.2247191011235], [30.0, 487.4030612244899]], "isOverall": false, "label": "webservice_request", "isController": false}, {"data": [[20.07881301850673, 503.05328653477915]], "isOverall": false, "label": "webservice_request-Aggregated", "isController": false}], "supportsControllersDiscrimination": true, "maxX": 30.0, "title": "Time VS Threads"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    axisLabel: "Number of active threads",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response times in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: { noColumns: 2,show: true, container: '#legendTimeVsThreads' },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s: At %x.2 active threads, Average response time was %y.2 ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesTimeVsThreads"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotTimesVsThreads"), dataset, options);
            // setup overview
            $.plot($("#overviewTimesVsThreads"), dataset, prepareOverviewOptions(options));
        }
};

// Time vs threads
function refreshTimeVsThreads(){
    var infos = timeVsThreadsInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTimeVsThreads");
        return;
    }
    if(isGraph($("#flotTimesVsThreads"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTimeVsThreads");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTimesVsThreads", "#overviewTimesVsThreads");
        $('#footerTimeVsThreads .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var bytesThroughputOverTimeInfos = {
        data : {"result": {"minY": 1608.1, "minX": 1.63577886E12, "maxY": 229459.23333333334, "series": [{"data": [[1.63577892E12, 105309.93333333333], [1.63577886E12, 22307.233333333334], [1.63577898E12, 1608.1]], "isOverall": false, "label": "Bytes received per second", "isController": false}, {"data": [[1.63577892E12, 229459.23333333334], [1.63577886E12, 48600.5], [1.63577898E12, 3503.983333333333]], "isOverall": false, "label": "Bytes sent per second", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.63577898E12, "title": "Bytes Throughput Over Time"}},
        getOptions : function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity) ,
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Bytes / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendBytesThroughputOverTime'
                },
                selection: {
                    mode: "xy"
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y"
                }
            };
        },
        createGraph : function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesBytesThroughputOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotBytesThroughputOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewBytesThroughputOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Bytes throughput Over Time
function refreshBytesThroughputOverTime(fixTimestamps) {
    var infos = bytesThroughputOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, -25200000);
    }
    if(isGraph($("#flotBytesThroughputOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesBytesThroughputOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotBytesThroughputOverTime", "#overviewBytesThroughputOverTime");
        $('#footerBytesThroughputOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var responseTimesOverTimeInfos = {
        data: {"result": {"minY": 469.65434380776327, "minX": 1.63577886E12, "maxY": 1335.0512820512822, "series": [{"data": [[1.63577892E12, 497.42325763508205], [1.63577886E12, 469.65434380776327], [1.63577898E12, 1335.0512820512822]], "isOverall": false, "label": "webservice_request", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.63577898E12, "title": "Response Time Over Time"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average response time was %y ms"
                }
            };
        },
        createGraph: function() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Times Over Time
function refreshResponseTimeOverTime(fixTimestamps) {
    var infos = responseTimesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyResponseTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, -25200000);
    }
    if(isGraph($("#flotResponseTimesOverTime"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimesOverTime", "#overviewResponseTimesOverTime");
        $('#footerResponseTimesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var latenciesOverTimeInfos = {
        data: {"result": {"minY": 469.4029574861368, "minX": 1.63577886E12, "maxY": 1334.8461538461538, "series": [{"data": [[1.63577892E12, 497.16092404072043], [1.63577886E12, 469.4029574861368], [1.63577898E12, 1334.8461538461538]], "isOverall": false, "label": "webservice_request", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.63577898E12, "title": "Latencies Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average response latencies in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendLatenciesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average latency was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesLatenciesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotLatenciesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewLatenciesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Latencies Over Time
function refreshLatenciesOverTime(fixTimestamps) {
    var infos = latenciesOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyLatenciesOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, -25200000);
    }
    if(isGraph($("#flotLatenciesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesLatenciesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotLatenciesOverTime", "#overviewLatenciesOverTime");
        $('#footerLatenciesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var connectTimeOverTimeInfos = {
        data: {"result": {"minY": 333.17005545286486, "minX": 1.63577886E12, "maxY": 1185.9487179487178, "series": [{"data": [[1.63577892E12, 359.6041503523888], [1.63577886E12, 333.17005545286486], [1.63577898E12, 1185.9487179487178]], "isOverall": false, "label": "webservice_request", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.63577898E12, "title": "Connect Time Over Time"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getConnectTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Average Connect Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendConnectTimeOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Average connect time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesConnectTimeOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotConnectTimeOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewConnectTimeOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Connect Time Over Time
function refreshConnectTimeOverTime(fixTimestamps) {
    var infos = connectTimeOverTimeInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyConnectTimeOverTime");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, -25200000);
    }
    if(isGraph($("#flotConnectTimeOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesConnectTimeOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotConnectTimeOverTime", "#overviewConnectTimeOverTime");
        $('#footerConnectTimeOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var responseTimePercentilesOverTimeInfos = {
        data: {"result": {"minY": 1.7976931348623157E308, "minX": 1.7976931348623157E308, "maxY": 4.9E-324, "series": [{"data": [], "isOverall": false, "label": "Max", "isController": false}, {"data": [], "isOverall": false, "label": "90th percentile", "isController": false}, {"data": [], "isOverall": false, "label": "99th percentile", "isController": false}, {"data": [], "isOverall": false, "label": "95th percentile", "isController": false}, {"data": [], "isOverall": false, "label": "Min", "isController": false}, {"data": [], "isOverall": false, "label": "Median", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 4.9E-324, "title": "Response Time Percentiles Over Time (successful requests only)"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true,
                        fill: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Response Time in ms",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: '#legendResponseTimePercentilesOverTime'
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s : at %x Response time was %y ms"
                }
            };
        },
        createGraph: function () {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesResponseTimePercentilesOverTime"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotResponseTimePercentilesOverTime"), dataset, options);
            // setup overview
            $.plot($("#overviewResponseTimePercentilesOverTime"), dataset, prepareOverviewOptions(options));
        }
};

// Response Time Percentiles Over Time
function refreshResponseTimePercentilesOverTime(fixTimestamps) {
    var infos = responseTimePercentilesOverTimeInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, -25200000);
    }
    if(isGraph($("#flotResponseTimePercentilesOverTime"))) {
        infos.createGraph();
    }else {
        var choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimePercentilesOverTime", "#overviewResponseTimePercentilesOverTime");
        $('#footerResponseTimePercentilesOverTime .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var responseTimeVsRequestInfos = {
    data: {"result": {"minY": 452.0, "minX": 1.0, "maxY": 9718.5, "series": [{"data": [[2.0, 902.5], [3.0, 826.0], [4.0, 457.0], [6.0, 472.0], [7.0, 460.0], [8.0, 463.0], [9.0, 466.0], [10.0, 458.5], [11.0, 465.0], [12.0, 471.0], [13.0, 455.0], [14.0, 470.0], [15.0, 456.0], [16.0, 455.0], [17.0, 454.0], [19.0, 463.0], [20.0, 454.5], [21.0, 460.5], [22.0, 455.5], [23.0, 458.0], [24.0, 460.0], [25.0, 467.0], [26.0, 459.0], [27.0, 457.0], [28.0, 454.0], [30.0, 455.0], [31.0, 460.0], [32.0, 461.0], [33.0, 453.0], [34.0, 455.0], [35.0, 452.0], [36.0, 456.0], [37.0, 479.0], [39.0, 457.5], [40.0, 464.0], [41.0, 524.0], [42.0, 471.0], [45.0, 530.0], [44.0, 524.5], [46.0, 501.0], [47.0, 506.5], [48.0, 467.0], [49.0, 516.5], [50.0, 487.5], [51.0, 506.0], [52.0, 481.5], [54.0, 462.5], [56.0, 478.0], [57.0, 492.0], [58.0, 465.0], [64.0, 465.5], [1.0, 9718.5]], "isOverall": false, "label": "Failures", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1000, "maxX": 64.0, "title": "Response Time Vs Request"}},
    getOptions: function() {
        return {
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Response Time in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: {
                noColumns: 2,
                show: true,
                container: '#legendResponseTimeVsRequest'
            },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median response time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesResponseTimeVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotResponseTimeVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewResponseTimeVsRequest"), dataset, prepareOverviewOptions(options));

    }
};

// Response Time vs Request
function refreshResponseTimeVsRequest() {
    var infos = responseTimeVsRequestInfos;
    prepareSeries(infos.data);
    if (isGraph($("#flotResponseTimeVsRequest"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesResponseTimeVsRequest");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotResponseTimeVsRequest", "#overviewResponseTimeVsRequest");
        $('#footerResponseRimeVsRequest .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};


var latenciesVsRequestInfos = {
    data: {"result": {"minY": 452.0, "minX": 1.0, "maxY": 9718.0, "series": [{"data": [[2.0, 902.5], [3.0, 826.0], [4.0, 456.0], [6.0, 471.5], [7.0, 460.0], [8.0, 462.5], [9.0, 466.0], [10.0, 458.5], [11.0, 465.0], [12.0, 471.0], [13.0, 455.0], [14.0, 469.5], [15.0, 456.0], [16.0, 455.0], [17.0, 454.0], [19.0, 463.0], [20.0, 454.0], [21.0, 460.5], [22.0, 455.5], [23.0, 457.0], [24.0, 459.0], [25.0, 467.0], [26.0, 459.0], [27.0, 457.0], [28.0, 454.0], [30.0, 454.5], [31.0, 459.0], [32.0, 461.0], [33.0, 453.0], [34.0, 455.0], [35.0, 452.0], [36.0, 456.0], [37.0, 478.5], [39.0, 457.0], [40.0, 464.0], [41.0, 524.0], [42.0, 471.0], [45.0, 529.0], [44.0, 524.0], [46.0, 501.0], [47.0, 506.5], [48.0, 467.0], [49.0, 516.5], [50.0, 487.5], [51.0, 506.0], [52.0, 481.0], [54.0, 462.5], [56.0, 478.0], [57.0, 492.0], [58.0, 464.5], [64.0, 465.0], [1.0, 9718.0]], "isOverall": false, "label": "Failures", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 1000, "maxX": 64.0, "title": "Latencies Vs Request"}},
    getOptions: function() {
        return{
            series: {
                lines: {
                    show: false
                },
                points: {
                    show: true
                }
            },
            xaxis: {
                axisLabel: "Global number of requests per second",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            yaxis: {
                axisLabel: "Median Latency in ms",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelFontFamily: 'Verdana, Arial',
                axisLabelPadding: 20,
            },
            legend: { noColumns: 2,show: true, container: '#legendLatencyVsRequest' },
            selection: {
                mode: 'xy'
            },
            grid: {
                hoverable: true // IMPORTANT! this is needed for tooltip to work
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s : Median Latency time at %x req/s was %y ms"
            },
            colors: ["#9ACD32", "#FF6347"]
        };
    },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesLatencyVsRequest"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotLatenciesVsRequest"), dataset, options);
        // setup overview
        $.plot($("#overviewLatenciesVsRequest"), dataset, prepareOverviewOptions(options));
    }
};

// Latencies vs Request
function refreshLatenciesVsRequest() {
        var infos = latenciesVsRequestInfos;
        prepareSeries(infos.data);
        if(isGraph($("#flotLatenciesVsRequest"))){
            infos.createGraph();
        }else{
            var choiceContainer = $("#choicesLatencyVsRequest");
            createLegend(choiceContainer, infos);
            infos.createGraph();
            setGraphZoomable("#flotLatenciesVsRequest", "#overviewLatenciesVsRequest");
            $('#footerLatenciesVsRequest .legendColorBox > div').each(function(i){
                $(this).clone().prependTo(choiceContainer.find("li").eq(i));
            });
        }
};

var hitsPerSecondInfos = {
        data: {"result": {"minY": 0.15, "minX": 1.63577886E12, "maxY": 42.86666666666667, "series": [{"data": [[1.63577892E12, 42.86666666666667], [1.63577886E12, 9.216666666666667], [1.63577898E12, 0.15]], "isOverall": false, "label": "hitsPerSecond", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.63577898E12, "title": "Hits Per Second"}},
        getOptions: function() {
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of hits / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendHitsPerSecond"
                },
                selection: {
                    mode : 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y.2 hits/sec"
                }
            };
        },
        createGraph: function createGraph() {
            var data = this.data;
            var dataset = prepareData(data.result.series, $("#choicesHitsPerSecond"));
            var options = this.getOptions();
            prepareOptions(options, data);
            $.plot($("#flotHitsPerSecond"), dataset, options);
            // setup overview
            $.plot($("#overviewHitsPerSecond"), dataset, prepareOverviewOptions(options));
        }
};

// Hits per second
function refreshHitsPerSecond(fixTimestamps) {
    var infos = hitsPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, -25200000);
    }
    if (isGraph($("#flotHitsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesHitsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotHitsPerSecond", "#overviewHitsPerSecond");
        $('#footerHitsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
}

var codesPerSecondInfos = {
        data: {"result": {"minY": 0.65, "minX": 1.63577886E12, "maxY": 42.56666666666667, "series": [{"data": [[1.63577892E12, 42.56666666666667], [1.63577886E12, 9.016666666666667], [1.63577898E12, 0.65]], "isOverall": false, "label": "500", "isController": false}], "supportsControllersDiscrimination": false, "granularity": 60000, "maxX": 1.63577898E12, "title": "Codes Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of responses / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendCodesPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "Number of Response Codes %s at %x was %y.2 responses / sec"
                }
            };
        },
    createGraph: function() {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesCodesPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotCodesPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewCodesPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Codes per second
function refreshCodesPerSecond(fixTimestamps) {
    var infos = codesPerSecondInfos;
    prepareSeries(infos.data);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, -25200000);
    }
    if(isGraph($("#flotCodesPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesCodesPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotCodesPerSecond", "#overviewCodesPerSecond");
        $('#footerCodesPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var transactionsPerSecondInfos = {
        data: {"result": {"minY": 0.65, "minX": 1.63577886E12, "maxY": 42.56666666666667, "series": [{"data": [[1.63577892E12, 42.56666666666667], [1.63577886E12, 9.016666666666667], [1.63577898E12, 0.65]], "isOverall": false, "label": "webservice_request-failure", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.63577898E12, "title": "Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTransactionsPerSecond"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                }
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTransactionsPerSecond"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTransactionsPerSecond"), dataset, options);
        // setup overview
        $.plot($("#overviewTransactionsPerSecond"), dataset, prepareOverviewOptions(options));
    }
};

// Transactions per second
function refreshTransactionsPerSecond(fixTimestamps) {
    var infos = transactionsPerSecondInfos;
    prepareSeries(infos.data);
    if(infos.data.result.series.length == 0) {
        setEmptyGraph("#bodyTransactionsPerSecond");
        return;
    }
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, -25200000);
    }
    if(isGraph($("#flotTransactionsPerSecond"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTransactionsPerSecond");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTransactionsPerSecond", "#overviewTransactionsPerSecond");
        $('#footerTransactionsPerSecond .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

var totalTPSInfos = {
        data: {"result": {"minY": 0.65, "minX": 1.63577886E12, "maxY": 42.56666666666667, "series": [{"data": [], "isOverall": false, "label": "Transaction-success", "isController": false}, {"data": [[1.63577892E12, 42.56666666666667], [1.63577886E12, 9.016666666666667], [1.63577898E12, 0.65]], "isOverall": false, "label": "Transaction-failure", "isController": false}], "supportsControllersDiscrimination": true, "granularity": 60000, "maxX": 1.63577898E12, "title": "Total Transactions Per Second"}},
        getOptions: function(){
            return {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                xaxis: {
                    mode: "time",
                    timeformat: getTimeFormat(this.data.result.granularity),
                    axisLabel: getElapsedTimeLabel(this.data.result.granularity),
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20,
                },
                yaxis: {
                    axisLabel: "Number of transactions / sec",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelFontFamily: 'Verdana, Arial',
                    axisLabelPadding: 20
                },
                legend: {
                    noColumns: 2,
                    show: true,
                    container: "#legendTotalTPS"
                },
                selection: {
                    mode: 'xy'
                },
                grid: {
                    hoverable: true // IMPORTANT! this is needed for tooltip to
                                    // work
                },
                tooltip: true,
                tooltipOpts: {
                    content: "%s at %x was %y transactions / sec"
                },
                colors: ["#9ACD32", "#FF6347"]
            };
        },
    createGraph: function () {
        var data = this.data;
        var dataset = prepareData(data.result.series, $("#choicesTotalTPS"));
        var options = this.getOptions();
        prepareOptions(options, data);
        $.plot($("#flotTotalTPS"), dataset, options);
        // setup overview
        $.plot($("#overviewTotalTPS"), dataset, prepareOverviewOptions(options));
    }
};

// Total Transactions per second
function refreshTotalTPS(fixTimestamps) {
    var infos = totalTPSInfos;
    // We want to ignore seriesFilter
    prepareSeries(infos.data, false, true);
    if(fixTimestamps) {
        fixTimeStamps(infos.data.result.series, -25200000);
    }
    if(isGraph($("#flotTotalTPS"))){
        infos.createGraph();
    }else{
        var choiceContainer = $("#choicesTotalTPS");
        createLegend(choiceContainer, infos);
        infos.createGraph();
        setGraphZoomable("#flotTotalTPS", "#overviewTotalTPS");
        $('#footerTotalTPS .legendColorBox > div').each(function(i){
            $(this).clone().prependTo(choiceContainer.find("li").eq(i));
        });
    }
};

// Collapse the graph matching the specified DOM element depending the collapsed
// status
function collapse(elem, collapsed){
    if(collapsed){
        $(elem).parent().find(".fa-chevron-up").removeClass("fa-chevron-up").addClass("fa-chevron-down");
    } else {
        $(elem).parent().find(".fa-chevron-down").removeClass("fa-chevron-down").addClass("fa-chevron-up");
        if (elem.id == "bodyBytesThroughputOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshBytesThroughputOverTime(true);
            }
            document.location.href="#bytesThroughputOverTime";
        } else if (elem.id == "bodyLatenciesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesOverTime(true);
            }
            document.location.href="#latenciesOverTime";
        } else if (elem.id == "bodyCustomGraph") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCustomGraph(true);
            }
            document.location.href="#responseCustomGraph";
        } else if (elem.id == "bodyConnectTimeOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshConnectTimeOverTime(true);
            }
            document.location.href="#connectTimeOverTime";
        } else if (elem.id == "bodyResponseTimePercentilesOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimePercentilesOverTime(true);
            }
            document.location.href="#responseTimePercentilesOverTime";
        } else if (elem.id == "bodyResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeDistribution();
            }
            document.location.href="#responseTimeDistribution" ;
        } else if (elem.id == "bodySyntheticResponseTimeDistribution") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshSyntheticResponseTimeDistribution();
            }
            document.location.href="#syntheticResponseTimeDistribution" ;
        } else if (elem.id == "bodyActiveThreadsOverTime") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshActiveThreadsOverTime(true);
            }
            document.location.href="#activeThreadsOverTime";
        } else if (elem.id == "bodyTimeVsThreads") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTimeVsThreads();
            }
            document.location.href="#timeVsThreads" ;
        } else if (elem.id == "bodyCodesPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshCodesPerSecond(true);
            }
            document.location.href="#codesPerSecond";
        } else if (elem.id == "bodyTransactionsPerSecond") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTransactionsPerSecond(true);
            }
            document.location.href="#transactionsPerSecond";
        } else if (elem.id == "bodyTotalTPS") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshTotalTPS(true);
            }
            document.location.href="#totalTPS";
        } else if (elem.id == "bodyResponseTimeVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshResponseTimeVsRequest();
            }
            document.location.href="#responseTimeVsRequest";
        } else if (elem.id == "bodyLatenciesVsRequest") {
            if (isGraph($(elem).find('.flot-chart-content')) == false) {
                refreshLatenciesVsRequest();
            }
            document.location.href="#latencyVsRequest";
        }
    }
}

/*
 * Activates or deactivates all series of the specified graph (represented by id parameter)
 * depending on checked argument.
 */
function toggleAll(id, checked){
    var placeholder = document.getElementById(id);

    var cases = $(placeholder).find(':checkbox');
    cases.prop('checked', checked);
    $(cases).parent().children().children().toggleClass("legend-disabled", !checked);

    var choiceContainer;
    if ( id == "choicesBytesThroughputOverTime"){
        choiceContainer = $("#choicesBytesThroughputOverTime");
        refreshBytesThroughputOverTime(false);
    } else if(id == "choicesResponseTimesOverTime"){
        choiceContainer = $("#choicesResponseTimesOverTime");
        refreshResponseTimeOverTime(false);
    }else if(id == "choicesResponseCustomGraph"){
        choiceContainer = $("#choicesResponseCustomGraph");
        refreshCustomGraph(false);
    } else if ( id == "choicesLatenciesOverTime"){
        choiceContainer = $("#choicesLatenciesOverTime");
        refreshLatenciesOverTime(false);
    } else if ( id == "choicesConnectTimeOverTime"){
        choiceContainer = $("#choicesConnectTimeOverTime");
        refreshConnectTimeOverTime(false);
    } else if ( id == "choicesResponseTimePercentilesOverTime"){
        choiceContainer = $("#choicesResponseTimePercentilesOverTime");
        refreshResponseTimePercentilesOverTime(false);
    } else if ( id == "choicesResponseTimePercentiles"){
        choiceContainer = $("#choicesResponseTimePercentiles");
        refreshResponseTimePercentiles();
    } else if(id == "choicesActiveThreadsOverTime"){
        choiceContainer = $("#choicesActiveThreadsOverTime");
        refreshActiveThreadsOverTime(false);
    } else if ( id == "choicesTimeVsThreads"){
        choiceContainer = $("#choicesTimeVsThreads");
        refreshTimeVsThreads();
    } else if ( id == "choicesSyntheticResponseTimeDistribution"){
        choiceContainer = $("#choicesSyntheticResponseTimeDistribution");
        refreshSyntheticResponseTimeDistribution();
    } else if ( id == "choicesResponseTimeDistribution"){
        choiceContainer = $("#choicesResponseTimeDistribution");
        refreshResponseTimeDistribution();
    } else if ( id == "choicesHitsPerSecond"){
        choiceContainer = $("#choicesHitsPerSecond");
        refreshHitsPerSecond(false);
    } else if(id == "choicesCodesPerSecond"){
        choiceContainer = $("#choicesCodesPerSecond");
        refreshCodesPerSecond(false);
    } else if ( id == "choicesTransactionsPerSecond"){
        choiceContainer = $("#choicesTransactionsPerSecond");
        refreshTransactionsPerSecond(false);
    } else if ( id == "choicesTotalTPS"){
        choiceContainer = $("#choicesTotalTPS");
        refreshTotalTPS(false);
    } else if ( id == "choicesResponseTimeVsRequest"){
        choiceContainer = $("#choicesResponseTimeVsRequest");
        refreshResponseTimeVsRequest();
    } else if ( id == "choicesLatencyVsRequest"){
        choiceContainer = $("#choicesLatencyVsRequest");
        refreshLatenciesVsRequest();
    }
    var color = checked ? "black" : "#818181";
    if(choiceContainer != null) {
        choiceContainer.find("label").each(function(){
            this.style.color = color;
        });
    }
}

